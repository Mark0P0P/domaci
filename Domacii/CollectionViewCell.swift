//
//  CollectionViewCell.swift
//  Domacii
//
//  Created by Amplitudo on 03/05/2019.
//  Copyright © 2019 Amplitudo. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var value1: UILabel!
    
    func updateView() {
        self.value1.text = "bla"
    }
    
}
