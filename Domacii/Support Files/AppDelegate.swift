//
//  AppDelegate.swift
//  Domacii
//
//  Created by Amplitudo on 29/04/2019.
//  Copyright © 2019 Amplitudo. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appCord = AppCoordinator()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        appCord.start()
        return true
    }

    
}

