//
//  AppCoordinator.swift
//  Domacii
//
//  Created by Amplitudo on 29/04/2019.
//  Copyright © 2019 Amplitudo. All rights reserved.
//

import UIKit

class AppCoordinator {
    
    let window: UIWindow
    init(window: UIWindow = UIWindow(frame: UIScreen.main.bounds)) {
        self.window = window
    }
    
    func start() {
        let vc = TabBar()
        window.rootViewController = vc
        window.makeKeyAndVisible()
    }
}
