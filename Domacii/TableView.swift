//
//  TableView.swift
//  Domacii
//
//  Created by Amplitudo on 29/04/2019.
//  Copyright © 2019 Amplitudo. All rights reserved.
//

import UIKit

class TableView: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
    
    }
    
}

extension TableView: UITableViewDelegate, UITableViewDataSource {
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    return 30
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if indexPath.row % 7 == 0 {
        let cell = Bundle.main.loadNibNamed("TableViewCell", owner: self, options: nil)?.first as! TableViewCell
    cell.value1.text = "BLA"
    cell.value2.text = "BLA"
    cell.value3.text = "BLA"
    
        return cell
    }
    let cell = Bundle.main.loadNibNamed("TableViewCell", owner: self, options: nil)?.first as! TableViewCell
    cell.value1.text = "bla"
    cell.value2.text = "bla"
    cell.value3.text = "bla"
    
    return cell
    }
    
    }



