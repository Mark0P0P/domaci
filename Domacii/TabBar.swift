//
//  TabBar.swift
//  Domacii
//
//  Created by Amplitudo on 29/04/2019.
//  Copyright © 2019 Amplitudo. All rights reserved.
//

import UIKit

class TabBar: UITabBarController {
    
    override func viewDidLoad() {
        super .viewDidLoad()
        makeTabBar()
        
        
    }
    
    func makeTabBar() {
        
        let firstController = TableView()
        firstController.tabBarItem.title = "Table"
        
        let secondController = CollectionView()
        secondController.tabBarItem.title = "Collection"
        
        let thirdController = ScrollView()
        thirdController.tabBarItem.title = "Scroll"
        
        viewControllers = [firstController, secondController, thirdController]
        
    }
}
